package fr.ensicaen.si.client;

import java.awt.BorderLayout;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import fr.ensicaen.si.client.components.UneditableTableModel;
import fr.ensicaen.si.client.listeners.AutoCompleteListener;
import fr.ensicaen.si.dao.client.ClientDao;
import fr.ensicaen.si.dao.client.DbClientDao;
import fr.ensicaen.si.dao.operation.DbOperationDao;
import fr.ensicaen.si.dao.operation.OperationDao;
import fr.ensicaen.si.model.Client;
import fr.ensicaen.si.model.Operation;
import fr.ensicaen.si.model.Result;

import java.awt.GridLayout;

@SuppressWarnings("serial")
public class Frame extends JFrame {

	private JPanel contentPane;
	private JTable operationTable;
	private JScrollPane clientPane;
	private JTable clientTable;
	private JScrollPane operationPane;
	private JPanel panel;
	private JTextField clientFirstName;
	private JTextField clientLastName;

	/**
	 * Create the frame.
	 */
	public Frame() {
		/* Get list of clients */
		ClientDao cd = ClientDao.getInstance();
		cd.setDelegate(new DbClientDao());
		final OperationDao od = OperationDao.getInstance();
		od.setDelegate(new DbOperationDao());

		List<Client> cls = cd.getClients();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 710, 471);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		//Clients
		clientPane = new JScrollPane();
		contentPane.add(clientPane, BorderLayout.CENTER);
		
		DefaultTableModel clientModel = new UneditableTableModel();
		
		clientTable = new JTable(clientModel);
		clientPane.setViewportView(clientTable);
		clientModel.addColumn("First"); 
		clientModel.addColumn("Last"); 
		
		for (Client cl : cls) { 
			clientModel.addRow(new Object[] {cl.getFirst(), cl.getLast()});
		}
		
		/* Operations */
		operationPane = new JScrollPane();
		contentPane.add(operationPane, BorderLayout.EAST);
		
		final DefaultTableModel operationModel = new UneditableTableModel(); 
		
		operationTable = new JTable(operationModel); 
		operationPane.setViewportView(operationTable);

		panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		panel.setLayout(new GridLayout(1, 0, 0, 0));
		
		/* Text fields */
		clientFirstName = new JTextField();
		panel.add(clientFirstName);
		clientFirstName.setColumns(10);

		clientLastName = new JTextField();
		panel.add(clientLastName);
		clientLastName.setColumns(10);
		operationModel.addColumn("Id"); 
		operationModel.addColumn("Card num"); 
		operationModel.addColumn("Account num"); 
		operationModel.addColumn("Amount"); 
		operationModel.addColumn("Date"); 

		/* Events */
		clientFirstName.getDocument().addDocumentListener(
				new AutoCompleteListener(clientLastName, 
						clientFirstName, clientTable, operationTable));
		clientLastName.getDocument().addDocumentListener(
				new AutoCompleteListener(clientLastName, 
						clientFirstName, clientTable, operationTable));
		
		clientTable.getSelectionModel().clearSelection();
		clientTable.getSelectionModel().addListSelectionListener(
			new ListSelectionListener() {
		        public void valueChanged(ListSelectionEvent event) {
		        	if (clientTable.getSelectedRow() >= 0) {
		        		operationModel.setRowCount(0);

		        		Result result = od.getByFullName(
			        			clientTable.getValueAt(clientTable.getSelectedRow(), 0).toString(), 
			        			clientTable.getValueAt(clientTable.getSelectedRow(), 1).toString());
			        	for(Operation o : result.getOperations()) {
			        		operationModel.addRow(new Object[] {
			        			o.getCardNum(), o.getAccountNum(), o.getAmount(), o.getDate()
			        		});
			        	}
		        	}
		        }
		    }
		);
	}
}
